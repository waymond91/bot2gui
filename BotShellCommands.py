import subprocess
import os
from botSocket import *

cwd = os.getcwd()
botname = 'jetson.local'
user = 'brett'
password = 'tarpKing'
location="/shellCommands"

def checkConnections():
    response = os.system("ping -i 0.2 -c 1 " + "jetson.local")
    if response == 0:
        bot = True
    else:
        bot = False
    return bot

#Starts the robots motion controller so its ready to receive trackballk commands
def botStart():
    send('bot-enable')
    print(receive())
#Stops the robots motion control so it won't receive motion commands
def botStop():
    send('bot-disable')
    print(receive())

#/home/brett/Documents/SLAB/botgui2/shell_commands/botStreamOpen.sh
def botStreamOpen():
    send('stream-open')

    subprocess.call("."+location+"/opencv_stream.py &", shell=True)

def botStreamClose():
    send('stream-close')
    subprocess.call("."+location+"/botStreamClose.sh", shell=True)

if __name__ == "__main__":
    send("get-state")
    print(receive())
    botStart()
    botStop()
    botStreamOpen()
    botStreamClose()
    send("get-state")
    print(receive())
