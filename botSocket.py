import socket
import atexit

host = socket.gethostname()

if host == 'g7':
    ports = [8002, 8003, 8004, 8005 ]
    ip = '127.0.0.1'
else:
    ports = [10000, 10001, 10002, 10003, 10004]
    ip = 'jetson.local'

for port in ports:
    try:
        fpvBotWifi = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        fpvBotWifi.connect((ip, port))
        print("Connected to port: ", port)
        break
    except socket.error:
        print("No such port: ",port)

def receive():
    message = ""
    data = str(fpvBotWifi.recv(1).decode('utf-8'))
    while(data !="\n"):
        message += data
        data = str(fpvBotWifi.recv(1).decode('utf-8'))
    return(message)

def send(message):
    fpvBotWifi.send((message+"\n").encode('utf-8'))

def exit_handler():
    fpvBotWifi.close()
atexit.register(exit_handler)

if __name__ == '__main__':
    send('get-state')
    print(receive())


