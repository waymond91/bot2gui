#! /home/slab/miniconda3/envs/cv/bin/python

import cv2
import numpy as np
import sys
import glob
import time
import atexit
import signal
import os
import datetime
import subprocess

cwd = os.getcwd()
temp_images = '/shell_commands/temp_images'
downsampling = 2
hd_location = "/media/slab/HD/Videos/"
start_time = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')
epoch_start = time.time()
subprocess.call("mkdir "+hd_location+start_time, shell=True)

class GSTCal:
    def __init__(self,usb_camera=True):
        # Gstreamer Pipeline Parameters
        self.usb_camera=usb_camera
        self.downsampling = downsampling

        # Camera Calibration Parameters
        # 160 Degree USB Camera
        if self.usb_camera:
            DIM=(1280, 720)
            K=np.array([[439.4229296523445, 0.0, 632.8386031774136], [0.0, 439.34234870037153, 365.46372888553526], [0.0, 0.0, 1.0]])
            D=np.array([[-0.17599970571455584], [0.024123302165824027], [-0.013156662663865734], [0.004592920493916963]])
            self.map1, self.map2 = cv2.fisheye.initUndistortRectifyMap(K, D, np.eye(3), K, DIM, cv2.CV_16SC2)
            #GStreamer Pipeline Parameters
            pipeline = 'udpsrc port=5000 caps = "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! decodebin ! videoconvert ! videoflip method=rotate-180 ! appsink'
        else:
            DIM=(1280, 720)
            K=np.array([[439.4229296523445, 0.0, 632.8386031774136], [0.0, 439.34234870037153, 365.46372888553526], [0.0, 0.0, 1.0]])
            D=np.array([[-0.17599970571455584], [0.024123302165824027], [-0.013156662663865734], [0.004592920493916963]])
            self.map1, self.map2 = cv2.fisheye.initUndistortRectifyMap(K, D, np.eye(3), K, DIM, cv2.CV_16SC2)
            pipeline = 'udpsrc port=5000 caps = "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! decodebin ! videoconvert ! videoflip method=rotate-180 ! appsink'
        self.cam = cv2.VideoCapture(pipeline, cv2.CAP_GSTREAMER)

        #atexit.register(self.exit_handler)
        signal.signal(signal.SIGTERM, self.exit_handler)
        self.count = 0
        self.last = time.time()
        self.frame_rate = []

    def exit_handler(self, signal, frame):
        print("Exiting")
        sys.exit(0)

    def capture(self):
        ret, img = self.cam.read()
        # Frame rate = 60 average
        # Lets save at 30 fps
        #fr = (1/(time.monotonic()-self.last))
        #self.frame_rate.append(fr)
        #print(sum(self.frame_rate) / len(self.frame_rate))
        #self.last=time.monotonic()
        if ret:
            #cv2.namedWindow('undistorted',cv2.WINDOW_NORMAL)
            #cv2.imshow("undistorted", img)
            #cv2.waitKey(1);
            self.undistort(img)

    def undistort(self, img):
        #img = cv2.imread(img_path)
        #h,w = img.shape[:2]
        if self.usb_camera:

            if self.count>self.downsampling:
                undistorted_img = cv2.remap(img, self.map1, self.map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
                #combined = np.concatenate((undistorted_img, img), axis=1)
                cv2.namedWindow('undistorted',cv2.WINDOW_NORMAL)
                cv2.imshow("undistorted", undistorted_img)
                cv2.waitKey(1);

                file_name = "img"+str(time.time()-epoch_start)[:-9]+".jpg"
                path = hd_location + start_time +'/'
                self.count = 0
                cv2.imwrite(path + file_name, undistorted_img)
        else:
            cv2.namedWindow('undistorted',cv2.WINDOW_NORMAL)
            cv2.imshow("undistorted", img)

            file_name = "img"+str(time.time())+".bmp"
            path = cwd + temp_images +'/'
            print(path)
            print(file_name)
            cv2.waitKey(1);
            if self.count>self.downsampling:
                self.count = 0
                #cv2.imwrite(path + file_name, img)

        self.count += 1

if __name__ == '__main__':
    pipeline = GSTCal()
    while True:
        pipeline.capture()
