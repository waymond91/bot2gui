# OMX, H264, RTSPPAY
#  rtph264depay ! h264parse ! queue ! avdec_h264 ! \
export OVERLAY=OMX_H264_RTSP
GST_TRACERS="interlatency" gst-launch-1.0 tcpserversrc host=jetson.local port=50000 ! \
  h264parse ! avdec_h264 ! \
  textoverlay text=$OVERLAY ! \
  xvimagesink sync=false async=false -e
