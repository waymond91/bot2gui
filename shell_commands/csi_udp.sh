# OMX, H264, RTSPPAY
#  rtph264depay ! h264parse ! queue ! avdec_h264 ! \
export OVERLAY=OMX_H264_RTSP
GST_TRACERS="interlatency" gst-launch-1.0 udpsrc port=5000 ! \
  application/x-rtp,encoding-name=H264,payload=96 ! \
  rtpjitterbuffer latency=0 ! rtph264depay ! h264parse ! avdec_h264 ! \
  textoverlay text=$OVERLAY ! \
  xvimagesink sync=false async=false -e
