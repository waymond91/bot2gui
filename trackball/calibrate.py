#! /home/slab/miniconda3/envs/mouse/bin/python

import _thread
from trackball import *

if __name__ == '__main__':

	xin1, xin2 = read_xinput()
	mouse0, mouse1 = connect_mice()
	if mouse0 and mouse1:
		_thread.start_new_thread(mouse0.readMouse,())
		_thread.start_new_thread(mouse1.readMouse,())
	else:
		print("Mice not connected!")
	def exit_handler(m1, m2):
		enable_mouse(m1)
		enable_mouse(m2)

	atexit.register(exit_handler, xin1, xin2)
	disable_mouse(xin1)
	disable_mouse(xin2)


	sumx0 = 0
	sumy0 = 0
	sumx1 = 0
	sumy1 = 0

	mouse = input('Please select mouse, enter "mouse0" or "mouse1": ')
	axis = input('Select axis, enter "y" or "x": ')
	input('Once completed, please update the parameter in the file /home/pi/Documents/SLAB/trackball/trackball.py starting on lines 10-13')

	while(1):
		time.sleep(0.05)
		dx0,dy0 = mouse0.getCoordinates()
		dx1,dy1 = mouse1.getCoordinates()

		sumx0 += dx0
		sumy0 += dy0
		sumx1 += dx1
		sumy1 += dy1

		if mouse == 'mouse0':
                    if axis == 'x':
                            print(sumx0)
                    if axis == 'y':
                            print(sumy0)
		if mouse == 'mouse1':
                    if axis == 'x':
                            print(sumx1)
                    if axis == 'y':
                            print(sumy1)
