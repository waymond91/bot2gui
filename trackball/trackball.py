#! /home/slab/miniconda3/envs/mouse/bin/python

import _thread
import linux_mouse
import time
import subprocess
import socket
import botModel
import os
import csv
import sys
import signal

# Trackball calibration data
mouse0_counts_x = 24843.0
mouse0_counts_y = 22905.0
mouse1_counts_x = 24843.0
mouse1_counts_y = 22905.0
circum_cm = 63.837

# Values for finding unique USB mice IDs
input_devices_path = "/proc/bus/input/devices"
identifier = "Uniq="
m0id = '4069-72-c4-d9-57'
m1id = '4069-cf-31-ca-b5'
# Wifi Socket configuration
host = socket.gethostname()

if host == 'g7':
    port_1 = 8000
    port_2 = 8001
    ip = '127.0.0.1'

else:
    port_1 = 10000
    port_2 = 10001
    ip = 'jetson.local'

BUFFER_SIZE = 1024

try:
    fpvBotWifi = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    fpvBotWifi.connect((ip, port_1))
except socket.error:
    fpvBotWifi = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    fpvBotWifi.connect((ip, port_2))


def wifiUpdate(r, l):
    correction_factor = 1.03
    message2 = 'R' + str(int(r)) + '!'
    message1 = 'L' + str(int(l * correction_factor)) + '!'

    print(message1)
    print(message2)

    fpvBotWifi.send(message1.encode('utf-8'))
    time.sleep(0.002)
    # response1 = fpvBotWifi.recv(BUFFER_SIZE)

    fpvBotWifi.send(message2.encode('utf-8'))


# response2 = fpvBotWifi.recv(BUFFER_SIZE)


def disable_mouse(id):
    id = str(id)
    subprocess.call('xinput set-prop ' + id + ' "Device Enabled" 0', shell=True)


def enable_mouse(id):
    id = str(id)
    subprocess.call('xinput set-prop ' + id + ' "Device Enabled" 1', shell=True)


# Read input device (/dev/inputs) and get unique mouse ids and input files
def parse_devices():
    unique_mice = []
    with open(input_devices_path) as f:
        file_list = f.readlines()

    count = 0
    for line in file_list:
        index = line.find(identifier)
        if index != -1:
            index += len(identifier)
            id = line[index:-1]
            if id:
                # print(id)
                # print(file_list[count+1])
                mouse_line = file_list[count + 1]
                index = mouse_line.find("mouse")
                if index != -1:
                    mouse_number = mouse_line[index + len("mouse"):index + len("mouse") + 1]
                    unique_mice.append((id, mouse_number))

                id = None
        count += 1
    return unique_mice


# Finds optical mice and connects to them
# Returns two linux_mouse objects
def connect_mice():
    mice = parse_devices()
    print(mice)
    for mouse in mice:
        id = mouse[0]
        num = str(mouse[1])
        if id == m0id:
            mouse0 = linux_mouse.linux_mouse('mouse' + num, 1.0)
            print("Mouse0 Connected!")
        if id == m1id:
            mouse1 = linux_mouse.linux_mouse("mouse" + num, 1.0)
            print("Mouse1 Connected!")
    if mouse0 and mouse1:
        return mouse0, mouse1
    else:
        return False


# Reads xinput --list so that mice may be disabled
def read_xinput():
    output = []
    xinputs = []
    cmd = ["xinput", "--list"]
    id_str = "tid="
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    for line in proc.stdout.readlines():
        output.append(line)
    for line in output:
        line = str(line)
        index = line.find("Logitech MX Master")
        if index != -1:
            index = line.find(id_str)
            if index != -1:
                if line[index + len(id_str) + 1:index + len(id_str) + 2].isdigit():
                    id = (line[index + len(id_str):index + len(id_str) + 2])
                else:
                    id = (line[index + len(id_str):index + len(id_str) + 1])
                xinputs.append(int(id))
    return (xinputs[0], xinputs[1])


if __name__ == '__main__':
    # Spawn new threads to read mice
    # Disable optical mice and re-enable at exit
    csv_data = []
    xin1, xin2 = read_xinput()
    mouse0, mouse1 = connect_mice()
    bot = botModel.botModel(3, 14.4, 625)
    if mouse0 and mouse1:
        _thread.start_new_thread(mouse0.readMouse, ())
        _thread.start_new_thread(mouse1.readMouse, ())


        def exit_handler(self, signal):
            enable_mouse(xin1)
            enable_mouse(xin2)
            # print("Mice re-enabled.")
            header = ['dt(s)', 'Mouse0-x Arclen(cm)', 'Mouse1-x Arclen(cm)', 'Mouse0-y Arclen(cm)',
                      'Bot Displacement(cm)', 'Bot Rotation(rad)']

            print(len(csv_data))
            print(header)
            if os.path.exists(("trackball_data.csv")):
                os.remove('trackball_data.csv')
            with open("trackball_data.csv", 'w') as results:
                wr = csv.writer(results, lineterminator='\n')
                wr.writerow(header)
                wr.writerows(csv_data)
            sys.exit(0)

    # atexit.register(exit_handler, xin1, xin2)
    signal.signal(signal.SIGTERM, exit_handler)
    disable_mouse(xin1)
    disable_mouse(xin2)

    sumx0 = 0
    sumy0 = 0
    sumx1 = 0
    sumy1 = 0
    last_time = time.time()

    while (1):
        time.sleep(0.05)
        data = []

        # calculate time since last loop
        now = time.time()
        dt = now - last_time
        last_time = now
        # print(dt)

        data.append(dt)

        dx0, dy0 = mouse0.getCoordinates()
        dx1, dy1 = mouse1.getCoordinates()

        # Scale inputs
        scaled_dx0 = dx0 * circum_cm / mouse0_counts_x
        scaled_dx1 = dx1 * circum_cm / mouse1_counts_x
        scaled_dy0 = dy0 * circum_cm / mouse0_counts_y

        data.append(scaled_dx0)
        data.append(scaled_dx1)
        data.append(scaled_dy0)

        # Calculate unicycle outputs, append to csv data
        s = (scaled_dx0 ** 2 + scaled_dx1 ** 2) ** (0.5)
        w = -1.0 * (dy0 * circum_cm / mouse0_counts_y) / 10.16
        if (dx1 < 0):
            s *= -1.0
            data.append(s)
            data.append(w)
        csv_data.append(data)

        r, l = bot.getWheelDeltas(s, w)
        if (r != 0 or l != 0):
            wifiUpdate(r, l)

        sumx0 += dx0
        sumy0 += dy0
        sumx1 += dx1
        sumy1 += dy1
