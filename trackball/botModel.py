#! /home/slab/miniconda3/envs/mouse/bin/python

import time
import sys
import struct
import numpy as np

step_size = 14
sleep_time = .001

x0 = 0
y0 = 0
p0 = 0
x = 0
y = 0
p = 0
dpx = 50#10.2857
dpy = 50#9.00

#Heading deltas
dp = 0
dp0 = 0

class botModel():
    def __init__(self, R, L, CPR):
        self.L = float(L)
        self.R = float(R)
        self.CPR = int(CPR)
        self.p = 0.0
        self.p0 = 0.0
        self.x = 0.0
        self.y = 0.0

    #Returns unicycle moodel coordinates (linear & angular displacement)
    #based off a change of cartesian position deltas in centimeters
    def cartesian2unicycle(self, x, y):
        global dp, dp0
        s = (x**2 + y**2)**(0.5)

        dp0 = dp
        dp = np.arctan2(y,x)

        self.p0 = self.p

        phi  = dp - dp0
        if (abs(phi) > np.pi):
            _dp = 2*np.pi - abs(phi)
        self.p += (phi)

        print(self.p)

        return s , phi

    #Takes a displacement in centimeters (dS)
    #and an angular velocity in rad/sec (dP)
    #and returns the encoder counts for each wheel.
    def getWheelDeltas(self, dS, dP):
        sr = (2*dS+dP*self.L)/(2*self.R)
        sl = (2*dS-dP*self.L)/(2*self.R)
        sr *= self.CPR/(2*np.pi)
        sl *= self.CPR/(2*np.pi)
        return sr, sl

    def getWheelSpeed(self, v, w):
        vr = (2*v+self.L*w)/(2*self.R)
        vl = (2*v - self.L*w)/(2*self.R)
        vr *= self.CPR/(2*np.pi)
        vl *= self.CPR/(2*np.pi)
        return(vr,vl)

    # Updates the position of the robot based off the changes in encoder counts
    def updatePosition(self, dR, dL):
        global x, y, p, x0, y0, p0
        dR = 2*np.pi*self.R*dR/self.CPR
        dL = 2*np.pi*self.R*dL/self.CPR
        dC = (dR+dL)/2

        p0 = pt
        p = p0 + (dR - dL)/2

        x0 = x
        x = x0 + dC*np.cos(p)

        y0 = y
        y = y0 + dC*np.sin(p)

        return x,y,p

    def angularDisplacement(self, dx, dy):
        w = dx * -1.0
        w /= 500
        r, l = self.getWheelDeltas(float(0), w)
        return r, l

    def linearDisplacement(self, dx, dy):
        s = (dx**2 + dy **2)**(0.5)
        s /= 5.0
        r, l = self.getWheelDeltas(s, float(0))

        return r, l

if __name__ == '__main__':
    """
	unicycle = botModel(4.0, 19.4, 3400)
	#mouse1 = linuxMouse('mouse0')
	_s, _p = unicycle.cartesian2unicycle(.1, -.1)
        print(_s,_p)
        _r, _l = unicycle.getWheelDeltas(_s, _p)
        print(_r, _l)

        while(1):
		count = 0
		while(count < 200):
			_s, _p = unicycle.cartesian2unicycle(.1, -.1)
			#print(_s, _p)
			_r, _l = unicycle.getWheelDeltas(_s, _p)
			update(_r, _l)
			time.sleep(.01)
			count += 1

		count = 0
		while(count < 200):
			_s, _p = unicycle.cartesian2unicycle(0, .1)
			#print(_s, _p)
			_r, _l = unicycle.getWheelDeltas(_s, _p)
			update(_r, _l)
			time.sleep(.01)
			count += 1

		count = 0
		while(count < 200):
			_s, _p = unicycle.cartesian2unicycle(-.1, 0)
			#print(_s, _p)
			_r, _l = unicycle.getWheelDeltas(_s, _p)
			update(_r, _l)
			time.sleep(0.01)
			count += 1


	ser.close()
    """
