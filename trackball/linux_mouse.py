import struct

class linux_mouse():
    #devInput is a mouse input in the folder /dev/input
    def __init__(self, devInput, scale):
        self.mouseFile = open("/dev/input/"+devInput,"rb")
        self.scale = scale
        #Sum of all X displacements
        self.X = 0
        #Sum of all Y displacements
        self.Y = 0

        self.x = 0
        self.y = 0

        self.ly = 0
        self.lx = 0

        self.dx = 0
        self.dy = 0

    def getMouseEvent(self):
        buf = self.mouseFile.read(3);
        _x,_y = struct.unpack( "bb", buf[1:] );
        #print(_x, _y)
        return _x, _y;

    def getCoordinates(self):
        self.lx = self.x
        self.ly = self.y

        self.x = self.X
        self.y = self.Y

        self.dx = self.x - self.lx
        self.dy = self.y - self.ly

        return (self.dx, self.dy)

    #This is a blocking read so we need multi-threading
    def readMouse(self):
    	while( 1 ):
    		_x,_y = self.getMouseEvent();
    		self.X += (self.scale * _x);
    		self.Y += (self.scale * _y);
