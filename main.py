#! /home/slab/miniconda3/envs/GuiEnv/bin/python

import sys
import datetime
import os
import csv
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import QtWidgets
from PyQt5.QtCore import *
from PyQt5 import QtCore
import BotShellCommands
import subprocess
import time

cwd = os.getcwd()
temp_images = 'temp_images' #This is stupid
bot_network_name = "BotSpot"
wireless_card = "wlp6s0"

class App(QWidget):

    streaming = False
    bot_connected = False
    bot_running = False
    trackball_connected = False
    trackball_running = False

    def __init__(self):
        super().__init__()
        self.title = 'SLAB FPV Bot'
        self.left = 10
        self.top = 10
        self.width = 320
        self.height = 550
        self.bsc = BotShellCommands.BotShellCommands('/shell_commands')

        self.initUI()

    def initUI(self):
        #App Layout
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        label = QLabel(self)
        pixmap = QPixmap('background.png')
        label.setPixmap(pixmap)
        label.setAlignment(Qt.AlignCenter)

        self.connectionError = QtWidgets.QErrorMessage()
        self.show()

        #Bot Controls
        self.bot_status_label = QLabel("Robot: Connected", self)
        self.bot_status_label.setAlignment(Qt.AlignCenter)

        self.stream_button = QPushButton('Start Stream', self)
        self.stream_button.clicked.connect(self.stream_handler)

        self.bot_button = QPushButton('Start Bot',self)
        self.bot_button.clicked.connect(self.bot_handler)

        breboot_button = QPushButton('Reboot Bot', self)
        breboot_button.clicked.connect(self.breboot_handler)

        #Trackball Controls
        self.ball_status_label = QLabel("Trackball Control: Connected", self)
        self.ball_status_label.setAlignment(Qt.AlignCenter)

        #Add widgets to layout

        self.layout = QGridLayout()
        self.layout.addWidget(label, 0, 0)
        self.layout.addWidget(self.bot_status_label, 1, 0)
        self.layout.addWidget(self.stream_button, 2, 0)
        self.layout.addWidget(self.bot_button, 3, 0)
        #self.layout.addWidget(breboot_button, 5, 0)
        #self.layout.addWidget(self.ball_status_label, 6, 0)

        #Parse wifi network
        cmd = ["iwconfig", wireless_card]
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        connected = False
        for line in proc.stdout.readlines():
            if bot_network_name in str(line):
                connected = True
                self.bot_connected = True
                print(line)

        if not connected:
            self.bot_status_label.setText("Not Connected. Please Connect and restart this application.")
            message = "Not connected to BotSpot network! \n Please connect to network and restart GUI."
            self.connectionError.showMessage(message)
            #sys.exit(0)

        self.setLayout(self.layout)



        ##Initialize timer that checks wireless connectivity
        #self.timer  = QtCore.QTimer(self)
        #self.timer.setInterval(50000)
        #self.timer.timeout.connect(self.check_connection)
        #self.check_connection()
        #self.timer.start()

    @pyqtSlot()
    def check_connection(self):
        bot = self.bsc.checkConnections()
        if bot:
            self.bot_connected = True
            self.trackball_connected = True
            self.bot_status_label.setText("Robot: Connected")
        else:
            self.bot_connected = False
            self.bot_status_label.setText("Robot: Disconnected")
        """
        if ball:
            self.trackball_connected = True
            self.ball_status_label.setText("Trackball Control: Connected")
        else:
            self.ball_status_label.setText("Trackball Control: Disconnected")
            self.trackball_connected = False
        """


    @pyqtSlot()
    def stream_handler(self):
        if self.bot_connected:
            if self.streaming:
				#Stop video stream
                self.bsc.botStreamClose()
                self.stream_button.setText('Start Stream')
                self.streaming = False

				#Get frame render times & save to CSV
				#Copy video frames to hard drive and title folder
                """
                if self.save_directory:
                    subprocess.call('cp '+cwd+"/shell_commands/"+temp_images+'/img* ' +  self.save_directory+"/",shell=True)
                    self.save_directory = None
                """

            else:
                #Save start time
                #self.save_directory = str(QFileDialog.getExistingDirectory(self, "Select Video Stream Save Location"))
                """
                if self.save_directory:

                    start_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
                    f = open(self.save_directory+'/start_time.txt' , 'w')
                    f.write(start_time)
                    f.close()
                """
                #Clear temp images
                subprocess.call('rm '+cwd+"/shell_commands/"+temp_images+'/img* ' ,shell=True)

                #Open Video Stream
                self.stream_button.setText('Stop Stream')
                self.bsc.botStreamOpen()
                self.streaming = True
            print('Stream Status: ', self.streaming)
        else:
            self.connectionError.showMessage("Bot not connected to network!")

    @pyqtSlot()
    def bot_handler(self):
        if self.bot_connected:
                if self.bot_running:
                    #Stop the bot
                    self.bsc.botStop()
                    self.bot_running = False
                    self.bot_button.setText('Start Bot')
                    #Stop the trackball
                    subprocess.call("sudo kill 9 $(pgrep trackball.py)", shell=True)
                    time.sleep(2)

                    if self.trackball_directory:
                        subprocess.call('mv '+cwd+'/trackball_data.csv '+self.trackball_directory+"/",shell=True)
                        self.trackball_directory = None


                else:
                    self.trackball_directory = str(QFileDialog.getExistingDirectory(self, "Select Trackball Data Location"))
                    #Start the bot
                    self.bsc.botStart()
                    self.bot_running = True
                    self.bot_button.setText('Stop Bot')
                    time.sleep(3)
                    #Start the trackball
                    subprocess.call("sudo ./trackball.py &", shell=True)
                print('Bot Status: ', self.bot_running)
        else:
            self.connectionError.showMessage("Bot not connected to network!")

    @pyqtSlot()
    def breboot_handler(self):
        if self.bot_connected:
            self.bsc.botPiReboot()
            print('Bot Reboot Called')
        else:
            self.connectionError.showMessage("Bot not connected to network!")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

"""
!!!
## Put some headers in!
$$$
"""
