### Motor Tuning
[x] Adaptive PID tuning to Firmware  
[x] Adaptive PID tuning to Websocket  
[x] Adaptive PID tuning python application  
[x] Derivative filter testin on pid_client.py  
[x] Derivative filter on PID loop to Firmware  
[x] Maximum integral term  
[-] Zero-crossing detection on integral term     
[x] Match motor responses between motors  

### Jetsonbot Server
[x] Create on server script to access all bot functions remotely  
[-] Debug trackball to jetsonbot server communication  
[-] Daemonize pid_server.py  
[-] Merge 'jetson_commands' & 'commands'

### Desktop GUI
[x] Add gui.py back to project  
[-] Use tcp socket to access bot commands

### Trackball
[x] Move trackball scripts into separate folder




