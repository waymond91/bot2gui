/*
* PlatformIO, version 4.3.3
*/
#include "mbed.h"
#include "main.h"
#include "../lib/DELTA_QEI/DELTA_QEI.h"
#include "UART_ISR.h"
#include "PID.h"

#define DEBUG false

Ticker debug_ticker;
void pid_debug(void);

AnalogIn fb1 (PC_2);
AnalogIn fb2 (PC_1);
Ticker control_ticker;
DELTA_QEI wheel1(PC_4, PC_5, NC, 979.62, DELTA_QEI::X4_ENCODING);
DELTA_QEI wheel2(PB_0, PB_1, NC, 979.62, DELTA_QEI::X4_ENCODING);
PID pid1(0.005, 0.0, 0.025, &wheel1, &control_ticker, &fb1);
PID pid2(0.005, 0.0, 0.025, &wheel2, &control_ticker, &fb2);

Serial pc(PA_11, PA_12); //tx, rx;

DigitalOut m1in1 (PA_6);
DigitalOut m1in2 (PA_5);
PwmOut m1d1 (PC_8);
DigitalOut m2in1 (PA_2);
DigitalOut m2in2 (PA_1);
PwmOut m2d1 (PC_7);
PwmOut buzzer (PA_0);
AnalogIn bat (PC_0);

void control_loop(void);
void current_monitor(void);
void serial_update(void);
void mot1_set(float);
void mot2_set(float);

const float vpa = 0.525;
const float vref = 3.3;
const float amax = 0.5;
volatile float current1, current2;
volatile bool stall_detected = false;

const char end_char = '!';
const char left_char = 'L';
const char right_char = 'R';
const char pid_char = '$';
const char current_char = '~';

int main(){
  pc.baud(PC_BAUD);

  buzzer.period(0.0015);
  buzzer = 0.5;
  wait_us(500000);
  buzzer = 0;

  m1in1 = 0;
  m1in2 = 1;
  m1d1 = 1;
  m1d1.period(.00005);

  m2in1 = 0;
  m2in2 = 1;
  m2d1 = 1;
  m2d1.period(.00005);

  pc.attach(&rxIsr);
  control_ticker.attach_us(&control_loop, PID_PERIOD);

  if (DEBUG){
    pc.printf("Bot begin!");
    debug_ticker.attach_us(&pid_debug, 500000);
  }

  while(1) {
    serial_update();
  }
}

void pid_debug (void){
  pc.printf("1:,%2f,%2f,%2f\n", pid1.setpoint, pid1.error, pid1.out);
  pc.printf("2:,%2f,%2f,%2f\n", pid2.setpoint, pid2.error, pid2.out);
}

//This loop is referenced in PID.h
void control_loop(void){
  mot1_set(pid1.update());
  mot2_set(pid2.update());

  /*
    current1 = fb1.read()*vref/vpa;
    current2 = fb2.read()*vref/vpa;
    if(current1 > amax || current2 > amax){
        stall_detected = true;
        mot1_set(0.0);
        mot2_set(0.0);
    }
    if(!stall_detected){
        mot1_set(pid1.update());
        mot2_set(pid2.update());
    }else{
        pc.printf("Stall detected!");
        mot1_set(0.0);
        mot2_set(0.0);
    }
  */

}

// Updates PID setpoints from new serial input
void serial_update(void){
	//Only update if there is new line in the serial buffer
	//t.reset();
	if(newlineFlag == 1){
		string local;
		local = in1;
		newlineFlag = 0;

		//Check to see which wheel to update
		if((local.find(left_char) == 0) & (local.find(end_char) == local.length()-1)){
			local.erase(local.begin());
			local.erase(local.end()-1);
			int dl;
			dl = std::atoi(local.c_str());
			pid2.setpoint += dl;

      pc.printf("L:%i\r\n", dl);

		}

		if((local.find(right_char) == 0) & (local.find(end_char) == local.length()-1)){
			local.erase(local.begin());
			local.erase(local.end()-1);
			int dr;
			dr = std::atoi(local.c_str());
			pid1.setpoint -= dr;

      pc.printf("R:%i\r\n", dr);
		}

    if((local.find(pid_char) == 0) & (local.find(end_char) == local.length() - 1)){
      test_t pid_test;
      local.erase(local.begin());
      local.erase(local.end()-1);
      const char * c_str = local.c_str();
      int side;
      sscanf(c_str, "%d,%f,%f,%f,%f", &side, &pid_test.p, &pid_test.i, &pid_test.d, &pid_test.setpoint);

      if (side == 1){
        pid1.tune(pid_test);
      }else if(side == 2){
        pid2.tune(pid_test);
      }
    }
  }
}

void mot1_set(float out){
  //Set motor direction
  if(out < 0 ){
    m1in1 = 1;
    m1in2 = 0;
  }else{
    m1in1 = 0;
    m1in2 = 1;
  }

  m1d1.write(1.0 - fabs(out));

}
void mot2_set(float out){
  //Set motor direction
  if (out < 0){
    m2in1 = 1;
    m2in2 = 0;
  }else{
    m2in1 = 0;
    m2in2 = 1;
  }

  m2d1.write(1.0 - fabs(out));

}
