#ifndef SHIFT_ARRAY_H
#define SHIFT_ARRAY_H

#include "arm_math.h"

#define BUFFER_LENGTH BLOCK_SIZE

class SHIFT_ARRAY{
public:
    volatile float32_t buffer[BUFFER_LENGTH];

    SHIFT_ARRAY(){
        reset();
    }

    void shift_in(float new_val){
        for (int j= BUFFER_LENGTH; j> 0; j--){
            buffer[j] = buffer[j-1];
        }
        buffer[0] = new_val;
    }

    void reset(){
        for (int j= BUFFER_LENGTH; j> 0; j--){
            buffer[j] = 0.0;
        }
    }
};

#endif
