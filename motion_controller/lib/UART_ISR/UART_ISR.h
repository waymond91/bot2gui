#include "mbed.h"
#include "main.h"
#define PC_BAUD 115200
#define BUFF_WIDTH 30
#define BUFF_LEN 500

static char rxBuff[BUFF_LEN][BUFF_WIDTH];
static int buffX = 0;
static int buffY = 0;
static string in1, in2;
volatile static int newlineFlag = 0;

// Interrupt routine to read in data from serial port
void rxIsr() {
	char c;
	c = pc.getc();

	//pc.putc(c);

	rxBuff[buffY][buffX] = c;
	buffX ++;
	//Increment buffer & Check for overflow
	if((c == end_char) | (buffX > BUFF_WIDTH -2)){
		//Add string termination
		rxBuff[buffY][buffX] = '\0';
		in1 = rxBuff[buffY];
		newlineFlag = 1;
		buffY ++;
		buffX = 0;
	}
	if(buffY > BUFF_LEN -1){
		buffY = 0;
	}
}
