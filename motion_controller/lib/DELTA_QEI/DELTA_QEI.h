#ifndef DELTA_QEI_H
#define DELTA_QEI_H

#define BLOCK_SIZE 128

#include "mbed.h"
#include "SHIFT_ARRAY.h"
#include "arm_math.h"

#define PREV_MASK 0x1 //Mask for the previous state in determining direction of rotation.
#define CURR_MASK 0x2 //Mask for the current state in determining direction of rotation.
#define INVALID   0x3 //XORing two states where both bits have changed.

#define SAMPLE_PERIOD       500//0.000001 //S
#define NUM_TAPS            71

class DELTA_QEI {

public:

    typedef enum Encoding {

        X2_ENCODING,
        X4_ENCODING

    } Encoding;


    DELTA_QEI(PinName channelA, PinName channelB, PinName index, int pulsesPerRev, Encoding encoding = X2_ENCODING);


    void reset(void);
    int getCurrentState(void);
    int getPulses(void);
    int getRevolutions(void);
    float getSpeed(void);

private:

    void encode(void);
    void index(void);

    Encoding encoding_;

    InterruptIn channelA_;
    InterruptIn channelB_;
    InterruptIn index_;

    int pulsesPerRev_;
    int prevState_;
    int currState_;

    volatile int pulses_;
    volatile int revolutions_;

    Ticker ticker;
    SHIFT_ARRAY dPulses;
    void ticker_callback(void);
    volatile int lastPulses;
    volatile float32_t fir_out[BLOCK_SIZE];
    volatile float32_t firStateF32[BLOCK_SIZE + NUM_TAPS - 1];

    const float32_t firCoeffs32[NUM_TAPS] = {
      0.00075122, 0.0015052,  0.00226183, 0.003021,   0.00378258, 0.00454646,
      0.00531253, 0.00608066, 0.00685073, 0.00762262, 0.00839623, 0.00917141,
      0.00994807, 0.01072607, 0.01150529, 0.01228562, 0.01306693, 0.01384911,
      0.01463202, 0.01541554, 0.01619957, 0.01698396, 0.01776861, 0.01855338,
      0.01933815, 0.02012281, 0.02090723, 0.02169128, 0.02247484, 0.0232578,
      0.02404002, 0.02482138, 0.02560177, 0.02638105, 0.02715911, 0.02793582,
      0.02715911, 0.02638105, 0.02560177, 0.02482138, 0.02404002, 0.0232578,
      0.02247484, 0.02169128, 0.02090723, 0.02012281, 0.01933815, 0.01855338,
      0.01776861, 0.01698396, 0.01619957, 0.01541554, 0.01463202, 0.01384911,
      0.01306693, 0.01228562, 0.01150529, 0.01072607, 0.00994807, 0.00917141,
      0.00839623, 0.00762262, 0.00685073, 0.00608066, 0.00531253, 0.00454646,
      0.00378258, 0.003021,   0.00226183, 0.0015052,  0.00075122
    };
    arm_fir_instance_f32 low_pass_fir;


};

#endif /* DELTA_QEI_H */
