#include "DELTA_QEI.h"

DELTA_QEI::DELTA_QEI(PinName channelA,
  PinName channelB,
  PinName index,
  int pulsesPerRev,
  Encoding encoding) : channelA_(channelA), channelB_(channelB),
  index_(index) {

  pulses_       = 0;
  revolutions_  = 0;
  pulsesPerRev_ = pulsesPerRev;
  encoding_     = encoding;

  int chanA = channelA_.read();
  int chanB = channelB_.read();

  currState_ = (chanA << 1) | (chanB);
  prevState_ = currState_;

  channelA_.rise(this, &DELTA_QEI::encode);
  channelA_.fall(this, &DELTA_QEI::encode);

  if (encoding == X4_ENCODING) {
    channelB_.rise(this, &DELTA_QEI::encode);
    channelB_.fall(this, &DELTA_QEI::encode);
  }

  arm_fir_init_f32(&low_pass_fir, NUM_TAPS, (float32_t *)&firCoeffs32[0], (float32_t *)&firStateF32[0], BLOCK_SIZE);

  ticker.attach_us(this, &DELTA_QEI::ticker_callback, SAMPLE_PERIOD);
  //Index is optional.
  if (index !=  NC) {
    index_.rise(this, &DELTA_QEI::index);
  }

}

void DELTA_QEI::ticker_callback(void){
  int pulses = DELTA_QEI::getPulses();
  dPulses.shift_in(float(pulses-lastPulses));
  lastPulses = pulses;
}

float DELTA_QEI::getSpeed(void){
  arm_fir_f32(&low_pass_fir, (float32_t *) &dPulses.buffer, (float32_t *) &fir_out, BLOCK_SIZE);
  return fir_out[NUM_TAPS-1];
}

void DELTA_QEI::reset(void) {

    pulses_      = 0;
    revolutions_ = 0;

}

int DELTA_QEI::getCurrentState(void) {

    return currState_;

}

int DELTA_QEI::getPulses(void) {

    return pulses_;

}

int DELTA_QEI::getRevolutions(void) {

    return revolutions_;

}

void DELTA_QEI::encode(void) {

    int change = 0;
    int chanA  = channelA_.read();
    int chanB  = channelB_.read();

    //2-bit state.
    currState_ = (chanA << 1) | (chanB);

    if (encoding_ == X2_ENCODING) {

        //11->00->11->00 is counter clockwise rotation or "forward".
        if ((prevState_ == 0x3 && currState_ == 0x0) ||
                (prevState_ == 0x0 && currState_ == 0x3)) {

            pulses_++;

        }
        //10->01->10->01 is clockwise rotation or "backward".
        else if ((prevState_ == 0x2 && currState_ == 0x1) ||
                 (prevState_ == 0x1 && currState_ == 0x2)) {

            pulses_--;

        }

    } else if (encoding_ == X4_ENCODING) {

        //Entered a new valid state.
        if (((currState_ ^ prevState_) != INVALID) && (currState_ != prevState_)) {
            //2 bit state. Right hand bit of prev XOR left hand bit of current
            //gives 0 if clockwise rotation and 1 if counter clockwise rotation.
            change = (prevState_ & PREV_MASK) ^ ((currState_ & CURR_MASK) >> 1);

            if (change == 0) {
                change = -1;
            }

            pulses_ -= change;
        }

    }

    prevState_ = currState_;

}

void DELTA_QEI::index(void) {

    revolutions_++;

}
