#include "mbed.h"
#include "PinDefs.h"
#include "QEI.h"
#include "BiQuad.h"
#include "MovingAverage.h"
#define PID_BUFFER 5

extern void control_loop(void);
struct test_t {
  float p;
  float i;
  float d;
  float setpoint;
};

class PID{
public:
  volatile float kp, ki, kd;
  volatile float p = 0;
  volatile float i = 0;
  volatile float d = 0;
  volatile float out = 0;
  volatile float error = 0;
  volatile float input = 0;
  volatile float dInput = 0;
  volatile float eSum = 0;
  volatile float lastInput = 0;
  volatile float lastError = 0;
  volatile float setpoint = 0;
  volatile float maxIterm = 1.0;
  const double a0 = 0.06745508395870334;
  const double a1 = 0.13491016791740668;
  const double a2 = 0.06745508395870334;
  const double b1 = -1.1429772843080923;
  const double b2 = 0.41279762014290533;
  const int num_samples = 32;

  QEI *wheel;
  Ticker *ticker;
  BiQuad *LPF;
  MovingAverage <float>average_de(num_samples,0.0);

  PID(float _kp, float _ki, float _kd, QEI *_wheel, Ticker *_ticker){
    kp=_kp;
    ki=_ki;
    kd=_kd;
    wheel = _wheel;
    ticker = _ticker;
    BiQuad lpf(a0, a1, a2, b1, b2);
    LPF = &lpf;
    for (int j = 0; j<num_samples; j++){
      average_de.Insert(0.0);
    }
  }

  float update(void){

    input = wheel->getPulses();

    error = input - setpoint;
    if(abs(error) < PID_BUFFER){
      error = 0;
    }
    p = kp*error;


    dInput = (input - lastInput);
    average_de.Insert(dInput)

    d = 0; //kd*average_de.GetAverage();

    eSum += ki*error;
    if (abs(error) + abs(lastError) > abs(error + lastError)){
      eSum = 0;
    }
    eSum = clamp(eSum, maxIterm);

    out = p+eSum+d;

    out = clamp(out, 1.0);

    lastInput = input;
    lastError = error;

    return (out);
  }

  void set_tunings(float _kp, float _ki, float _kd){

    // update controller parameters
    kp=_kp;
    ki=_ki;
    kd=_kd;

    // Reset internal terms to avoid wind-up problems
    wheel->reset();
    p = 0;
    i = 0;
    d = 0;
    out = 0;
    error = 0;
    input = 0;
    dInput = 0;
    eSum = 0;
    lastInput = 0;
    setpoint = 0;


  }

  void print_state(void){
    pc.printf("%.2f,%.2f,%.2f,%.2f,%.2f\n", error, p, eSum, d, out);
  }

  void tune(test_t test_params){
    pc.printf("Time,Error,P,I,D,Out\n");

    float print_period = 1;
    float start_time = 0.5;
    float run_time = 1.5;

    set_tunings(test_params.p, test_params.i, test_params.d);

    // Restart control loop
    ticker->attach_us(&control_loop, 5000.0);

    Timer test_timer;
    test_timer.start();

    Timer print_timer;
    print_timer.start();

    while (test_timer.read() < start_time){
      if (print_timer.read_ms() > print_period){
          pc.printf("%.3f, ", test_timer.read());
          print_state();
          print_timer.reset();
      }
    }

    setpoint+= test_params.setpoint;

    while (test_timer.read() < run_time){
      if (print_timer.read_ms() > print_period){
          pc.printf("%.3f,", test_timer.read());
          print_state();
          print_timer.reset();
      }
    }
    //Turn off motors, detach control loop
    ticker -> detach();
    m1d1 = 1;
    m2d1 = 1;
  }

  float clamp(float in, float maxmin){
    float min = maxmin*-1.0;
    float max = maxmin;

    if (in < min){
      return(min);
    }

    if (in > max){
      return(max);
    }

    return in;

  }


};
