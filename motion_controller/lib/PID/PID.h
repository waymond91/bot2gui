#include "mbed.h"
#include "main.h"
#include "DELTA_QEI.h"

#define PID_ERROR_BUFFER 5

struct test_t {
  float p;
  float i;
  float d;
  float setpoint;
};

class PID{
public:
  volatile float kp, ki, kd;
  volatile float p = 0;
  volatile float eSum = 0;
  volatile float d = 0;
  volatile float out = 0;
  volatile float error = 0;
  volatile float input = 0;
  volatile float dInput = 0;
  volatile float lastInput = 0;
  volatile float setpoint = 0;
  volatile float amperage = 0;
  const float maxOut = 0.8; //0.75 removed flip with black battery
                            //0.8 for the blue battery
  volatile float maxIterm = maxOut;
  const float vpa = 0.525;
  const float vref = 3.3;
  const float amax = 0.5;

  DELTA_QEI *wheel;
  Ticker *ticker;
  AnalogIn *ain;

  PID(float _kp, float _ki, float _kd, DELTA_QEI *_wheel, Ticker *_ticker, AnalogIn* _ain){
    kp=_kp;
    ki=_ki;
    kd=_kd;
    ain = _ain;
    wheel = _wheel;
    ticker = _ticker;
    Ticker encoder_ticker;
  }


  float update(void){
    input = wheel->getPulses();

    error = input - setpoint;
    if(abs(error) < PID_ERROR_BUFFER){
      error = 0;
    }
    p = kp*error;


    dInput = wheel->getSpeed();//(input - lastInput);

    d = kd*dInput;
    //d = LPF -> step(d);

    eSum += ki*error;
    eSum = clamp(eSum, maxIterm);

    if ( (abs(input)+abs(lastInput)) > abs(input + lastInput) ){
      eSum = 0.0;
    }

    out = p+eSum+d;

    out = clamp(out, maxOut);

    lastInput = input;

    amperage = ain->read()*vref/vpa;

    return (out);
  }

  void set_tunings(float _kp, float _ki, float _kd){

    // update controller parameters
    kp=_kp;
    ki=_ki;
    kd=_kd;

    // Reset internal terms to avoid wind-up problems
    wheel->reset();
    p = 0;
    eSum = 0;
    d = 0;
    out = 0;
    error = 0;
    input = 0;
    dInput = 0;
    lastInput = 0;
    setpoint = 0;
  }

  void print_state(void){
    pc.printf("%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.4f\n", error, dInput, p, eSum, d, out, amperage);
  }

  void tune(test_t test_params){
    pc.printf("Time,Error,dError,P,I,D,Out,A\n");

    float print_period = 1;
    float start_time = 0.5;
    float run_time = 5;

    set_tunings(test_params.p, test_params.i, test_params.d);

    // Restart control loop
    ticker->attach_us(&control_loop, PID_PERIOD);

    Timer test_timer;
    test_timer.start();

    Timer print_timer;
    print_timer.start();

    while (test_timer.read() < start_time){
      if (print_timer.read_ms() > print_period){
          pc.printf("%.3f, ", test_timer.read());
          print_state();
          print_timer.reset();
      }
    }

    setpoint+= test_params.setpoint;

    while (test_timer.read() < run_time){
      if (print_timer.read_ms() > print_period){
          pc.printf("%.3f,", test_timer.read());
          print_state();
          print_timer.reset();
      }
    }
    //Turn off motors, detach control loop
    ticker -> detach();
    m1d1 = 1;
    m2d1 = 1;
  }

  float clamp(float in, float maxmin){
    float min = maxmin*-1.0;
    float max = maxmin;

    if (in < min){
      return(min);
    }

    if (in > max){
      return(max);
    }

    return in;

  }
};
