echo "____________________________________________________________________"
echo""
echo "WARNING: MAKE SURE TO STOP BOT BEFORE FIRMWARE UPDATE!!!"
echo ""
echo "Sucessful update should output:"
echo "Loading section .text, size 0x13dd8 lma 0x8000000"
echo "Loading section .ARM.exidx, size 0x8 lma 0x8013dd8"
echo "Loading section .data, size 0xad0 lma 0x8013de0"
echo "____________________________________________________________________"
echo ""
if pgrep -x "openocd" > /dev/null
then
	echo "OpenOCD is already running"
else
	echo "Starting Openocd..."
	./debug_init.sh &
fi

arm-none-eabi-gdb /home/brett/jetson_bot/motion_controller/.pio/build/nucleo_f411re/firmware.elf --command=upload_commands.gdb

