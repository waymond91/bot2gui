if pgrep -x "openocd" > /dev/null
then
	echo "OpenOCD is already running"
else
	echo "Starting Openocd..."
	./debug_init.sh &
fi

arm-none-eabi-gdb --command=stop_commands.gdb
