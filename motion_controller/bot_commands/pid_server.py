#! /usr/bin/python

import serial
import string
import sys
import time
import socket

port = 10001
ip = 'jetson.local'

ser = serial.Serial(
    port='/dev/ttyTHS1',
    baudrate=115200,
    bytesize=serial.EIGHTBITS,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    timeout=1,
    xonxoff=False,
    rtscts=False,
    dsrdtr=False,
    writeTimeout=2
)


class PidTest:
    start_char = '$'
    end_char = '!'
    test_time = 2

    def __init__(self, side, p, i, d, setpoint):
        self.side = side
        self.kp = p
        self.ki = i
        self.kd = d
        self.setpoint = setpoint
        self.command_str = self.start_char + \
            str(self.side) + "," + \
            str(self.kp) + "," + \
            str(self.ki) + "," + \
            str(self.kd) + "," + \
            str(self.setpoint) + \
            self.end_char

    def run(self):
        ser.write(self.command_str)
        print("Sent Command: "+self.command_str)

    def read(self):
        data = []
        t_end = time.time() + self.test_time
        print(time.time(), t_end)
        while time.time() < t_end:
            line = ser.readline()
            if line:
                data.append(line)
                sys.stdout.write(line)
        return data

def example():
    kps = [0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008]
    tests = []

    for kp in kps:
        tests.append(PidTest(2, kp, 0.0, 0.0, 315))

    for test in tests:
        test.run()
        test.read()

def main():
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (ip, port)#('jetson.local', port)
    print >>sys.stderr, 'starting up on %s port %s' % server_address
    sock.bind(server_address)

    while True:
        sock.listen(1)
        # Wait for a connection
        print >>sys.stderr, 'waiting for a connection'
        connection, client_address = sock.accept()

        try:
            print >>sys.stderr, 'connection from', client_address
            message = ""
            while True:
                data = str(connection.recv(1))
                if data == "":
                    break
                if data != "\n":
                    message += data
                else:
                    print(message)
                    #connection.sendall(message)
                    ser.write(message)
                    while(True):
                        line  = ser.readline()
                        if line:
                            print(line)
                            connection.send(line)
                        else:
                            break
                    message = ""
                    # side, kp, ki, kd, setpoints
                    # args = map(float, data.split(','))
                    # PidTest test(*args)
                    # test.run()
                    # data = test.read()
                    # connection.sendall(bytes(data))

        finally:
            print(message)
            connection.close()


if __name__ == '__main__':
    main()
