#include "mbed.h"
#define PID_PERIOD 5000 //microseconds

//DigitalIn dummy1 (PC_10);
//DigitalIn dummy2 (PC_11);
extern const char end_char;
extern const char left_char;
extern const char right_char;
extern const char test_command;
extern Serial pc;
extern DigitalOut m1in1;
extern DigitalOut m1in2;
extern PwmOut m1d1;
extern DigitalOut m2in1;
extern DigitalOut m2in2;
extern PwmOut m2d1;
extern PwmOut buzzer;
extern AnalogIn bat;
extern AnalogIn fb1;
extern AnalogIn fb2;

void mot1_set();
void mot2_set();
void control_loop(void);
