#! /home/slab/miniconda3/envs/kaggle/bin/python

import socket
import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal

host = socket.gethostname()

if host == 'g7':
    port_1 = 8000
    port_2 = 8001
    ip = '127.0.0.1'


else:
    port_1 = 10000
    port_2 = 10001
    ip = socket.gethostbyname('jetson.local')

BUFFER_SIZE = 4096#1024
CPR = 625.0
wd = 8.0 #cm
alternator = 1

class PidTest:
    start_char = '$'
    end_char = '!'
    test_time = 2

    def __init__(self, side, p, i, d, setpoint):
        self.side = side
        self.kp = round(p,8)
        self.ki = round(i,8)
        self.kd = round(d,8)
        self.setpoint = setpoint
        self.command_str = self.start_char + \
            str(self.side) + "," + \
            str(self.kp) + "," + \
            str(self.ki) + "," + \
            str(self.kd) + "," + \
            str(self.setpoint) + \
            self.end_char + '\n'

    def move_forward(self, distance):
        distance = str(distance)
        m1 = 'L' + distance + '!\n'
        m2 = 'R' + distance + '!\n'
        fpvBotWifi = None
        try:
            fpvBotWifi = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            fpvBotWifi.connect((ip, port_1))
            fpvBotWifi.settimeout(self.test_time)
        except socket.error:
            fpvBotWifi = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            fpvBotWifi.connect((ip, port_2))
            fpvBotWifi.settimeout(self.test_time)
        if fpvBotWifi:
            fpvBotWifi.send(m1.encode('utf-8'))
            time.sleep(0.002)
            fpvBotWifi.send(m2.encode('utf-8'))

    def run(self):
        fpvBotWifi = None
        try:
            fpvBotWifi = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            fpvBotWifi.connect((ip, port_1))
            fpvBotWifi.settimeout(self.test_time)
        except socket.error:
            fpvBotWifi = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            fpvBotWifi.connect((ip, port_2))
            fpvBotWifi.settimeout(self.test_time)
        if fpvBotWifi:
            fpvBotWifi.send(self.command_str.encode('utf-8'))

            #this is pretty ugly but works for now
            results = []
            while True:
                try:
                    line = fpvBotWifi.recv(BUFFER_SIZE)
                    try:
                        results.append(line.decode('utf').replace("\n", ""))
                    except UnicodeDecodeError:
                        pass
                    print(results[-1])
                except socket.timeout:
                    break

            fpvBotWifi.close()
            data_frame = []
            for line in results[1:-1]:
                vals = line.split(",")
                _floats = []
                for val in vals:
                    try:
                        val = float(val)
                        _floats.append(val)
                    except ValueError:
                        break

                #data_frame.append(float(val) for val in line.split(","))
                data_frame.append(_floats)
            print(results)
            column_names = results[0].split(",")
            df = pd.DataFrame(data_frame, columns=column_names)
            #df['Error'] = df['Error'].mul((3.14159*wd)/CPR)
            dError = df['Error'].diff()
            dt = df['Time'].diff()
            df['dE/dt'] = df['D']
            #df['dE/dt'] = dError/dt
            for index, row in df.iterrows():
                if row['Time']<0.51:
                    row['dE/dt'] = 0.0
            return(df)
        else:
            raise ConnectionError("Cannot connect to fpvbot server.")


def plot_results(dfs, titles, motor_id):
    fig, axs = plt.subplots(4, 1, figsize=(18, 10))
    for i, df in enumerate(dfs):
        axs[0].plot(df['Time'], df['Error'], label=titles[i])
    plt.legend()
    axs[0].title.set_text("Motor " + str(motor_id) + " Step Response")
    #plt.xlabel("Time (s)")
    axs[0].set_ylabel("Error (Encoder Counts)")

    for i, df in enumerate(dfs):
        axs[1].plot(df['Time'], df['dError'], label=titles[i])
    plt.legend()
    axs[1].title.set_text("Motor " + str(motor_id) + " Step Response")
    #plt.xlabel("Time (s)")
    axs[1].set_ylabel("dError (Encoder Counts /s)")

    n = 31
    b = signal.firwin(n, cutoff=0.5, window="hamming")
    for i, df in enumerate(dfs):
        #df['A'] = signal.lfilter(b, [1], df['A'])
        axs[2].plot(df['Time'], df['A'], label=titles[i])
    plt.legend()
    #plt.title("Motor " + str(motor_id) + " Step Response")
    #plt.xlabel("Time (s)")
    axs[2].set_ylabel("Current (A)")

    for i, df in enumerate(dfs):
        axs[3].plot(df['Time'], df['Out'], label=titles[i])
    plt.legend()
    #plt.title("Motor " + str(motor_id) + " Step Response")
    axs[3].set_xlabel("Time (s)")
    axs[3].set_ylabel("Controller Output")

    plt.show()


def joined_plot(left_dfs, left_titles, right_dfs, right_titles):
    fig, axs = plt.subplots(2, 1, figsize=(18, 10))
    for i, df in enumerate(left_dfs):
        axs[0].plot(df['Time'], df['Error'], label=left_titles[i])
    axs[0].title.set_text('Left Motor Step Response')
    axs[0].legend()

    for i, df in enumerate(right_dfs):
        df['Error'] *= -1
        axs[1].plot(df['Time'], df['Error'], label=right_titles[i])
    axs[1].title.set_text('Right Motor Step Response')
    axs[1].legend()

    plt.show()

def plot_controller_output(dfs, titles):
    fig, axs = plt.subplots(len(dfs), 1, figsize=(18,10))
    for i, df in enumerate(dfs):
        df.plot(kind='line',x='Time',y='Error',ax=axs[i])
    plt.show()

def kp_sweep(motor_id, start, stop):
    global alternator
    if motor_id == 2:
        setpoint = 315
    else:
        setpoint = -315
    #kps = [0.001, 0.005, 0.01, 0.015, 0.02]
    kps = np.linspace(start, stop, 5)
    tests = []

    for kp in kps:
        tests.append(PidTest(motor_id, kp, 0.0, 0.0, setpoint))
        #alternator *= -1.0

    dfs = []
    titles = []
    for test in tests:
        titles.append("KP: "+str(test.kp))
        df = test.run()
        time.sleep(1)
        dfs.append(df)
    return dfs, titles


def kd_sweep(motor_id, start, stop, kp):
    global alternator
    if motor_id == 2:
        setpoint = 315
    else:
        setpoint = -315
    kds = np.linspace(start, stop, 5)
    tests = []

    for kd in kds:
        tests.append(PidTest(motor_id, kp, 0.0, kd, setpoint))
        #alternator *= -1.0

    dfs = []
    titles = []
    for test in tests:
        titles.append("KD: "+str(test.kd))
        df = test.run()
        time.sleep(1)
        dfs.append(df)

    return dfs, titles


def ki_sweep(motor_id: object, start: object, stop: object, kp: object, kd: object) -> object:
    global alternator
    if motor_id == 2:
        setpoint = 100
    else:
        setpoint = -100
    kis = np.linspace(start, stop, 5)
    tests = []

    for ki in kis:
        tests.append(PidTest(motor_id, kp, ki, kd, setpoint))
        #alternator *= -1.0

    dfs = []
    titles = []
    for test in tests:
        titles.append("KI: "+str(test.ki))
        df = test.run()
        time.sleep(1)
        dfs.append(df)

    return dfs, titles


def setpoint_sweep(motor_id, start, stop, kp, ki, kd):
    setpoints = np.linspace(start, stop, 5)
    if motor_id == 1:
        setpoints *= -1
    tests = []

    for setpoint in setpoints:
        tests.append(PidTest(motor_id, kp, ki, kd, setpoint))

    dfs = []
    titles = []
    for test in tests:
        titles.append("Setpoint: " + str(test.setpoint))
        df = test.run()
        time.sleep(1)
        dfs.append(df)

    return dfs, titles


# %%
if __name__ == '__main__':
    right = 2
    left = 1
    #dfl, tl = setpoint_sweep(left, 10, 615, 0.1, 0.00001, 0.0)
    #dfr, tr = setpoint_sweep(right, 10, 615, 0.1, 0.00001, 0.0)
    #joined_plot(dfl, tl, dfr, tr)

    #dfs, titles = kp_sweep(left, 0.01,0.02)    #setpoint_sweep(left, 1, 100, 0.0175, 0.0, 0.1)
    dfs, titles = kp_sweep(left, 0.0015,0.005)
    #dfs, titles = kd_sweep(left, 0.4, 0.6, 0.004)
    #dfs, titles = ki_sweep(left, 0.00001, 0.0001, 0.004, 0.5)
    plot_results(dfs, titles, left)

    #dfl, tl = setpoint_sweep(left, 15, 615, 0.004, 0.000025, 0.5)
    #dfr, tr = setpoint_sweep(right, 15, 615, 0.004, 0.0000125, 0.5)
    #joined_plot(dfl, tl, dfr, tr)
    #kd_sweep(left, 0.01,0.2, 0.0134) #0.09,0.2 kp, kd
    #ki_sweep(right, 0.001, 0.01, 0.09, 0.2)
    #setpoint_sweep(left, 1, 100, 0.0175, 0.0, 0.1)  # looks pretty good!
    #dfs, titles  = setpoint_sweep(right, 615, 1615, 0.075, 0.00, 0.4) # looks pretty good!
    #plot_results(dfs, titles, right)




# %%

    #kp_sweep(left, 0.01,0.02)
    #kd_sweep(left, 0.1, 0.2, 0.0175)
    #ki_sweep(left, 0.0001, 0.0005, 0.0175, 0.15)
