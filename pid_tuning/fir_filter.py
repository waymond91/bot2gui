import pylab
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt

#Plot frequency and phase response
def mfreqz(b,a=1):
    w,h = signal.freqz(b,a)
    h_dB = 20 * np.log10 (abs(h))
    plt.subplot(211)
    plt.plot(w/max(w),h_dB)
    plt.ylim(-80, 5)
    plt.ylabel('Magnitude (db)')
    plt.xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    plt.title(r'Frequency response')
    plt.subplot(212)
    h_Phase = np.unwrap(np.arctan2(np.imag(h),np.real(h)))
    plt.plot(w/max(w),h_Phase)
    plt.ylabel('Phase (radians)')
    plt.xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    plt.title(r'Phase response')
    plt.subplots_adjust(hspace=0.5)
    plt.show()

#Plot step and impulse response
def impz(b,a=1):
    l = len(b)
    impulse = np.repeat(0.,l); 
    #impulse[0] =1.
    x = np.arange(0,l)
    response = signal.lfilter(b,a,impulse)
    plt.subplot(211)
    plt.stem(x, response)
    plt.ylabel('Amplitude')
    plt.xlabel(r'n (samples)')
    plt.title(r'Impulse response')
    plt.subplot(212)
    step = np.cumsum(response)
    plt.stem(x, step)
    plt.ylabel('Amplitude')
    plt.xlabel(r'n (samples)')
    plt.title(r'Step response')
    plt.subplots_adjust(hspace=0.5)
    plt.show()

if __name__ == '__main__':
    num_taps = 71
    fcutoff = 4
    fsample = 2000
    print(f'Filter Delay {(num_taps-1)/(2*fsample)}')

    b = signal.firwin(num_taps, cutoff = (fcutoff/(fsample/2)), window='triangle')
    print(b)
    mfreqz(b)
    #Frequency and phase response
    t = np.linspace(0, 1, fsample)
    fsig = 2
    fnoise = 200
    x = np.sin(2 * np.pi * fsig * t ) + 1.0*np.sin(2 * np.pi * fnoise * t )
    cleaned = signal.lfilter(b, [1], x)

    plt.plot(t, cleaned);
    plt.plot(t, x)
    plt.show()

    windows = [128]
    n = 0
    residuals = []
    plot = True
    for window_size in windows:
        while(window_size*n < fsample):
            sig = x[n*window_size:(n+1)*window_size]
            _t = t[n*window_size:(n+1)*window_size]
            clean = signal.lfilter(b, [1], sig)
            n += 1
            delay=int((num_taps-1))
            try:
                r = cleaned[n*window_size+delay]-clean[delay]
                residuals.append(abs(r))
            except IndexError:
                pass
            if plot:
                plt.plot(_t,sig)
                plt.plot(_t, clean)
                #plt.plot(_t, cleaned[n*window_size:(n+1)*window_size])
                plt.plot(_t[delay], clean[delay],'rx')
                plt.show()
        print(f'Window Size: {window_size}, Residuals: {np.average(residuals)}')
    #plt.plot(t, np.sin(2 * np.pi * fsig * t ))

